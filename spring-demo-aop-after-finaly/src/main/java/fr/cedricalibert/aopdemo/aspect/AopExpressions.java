package fr.cedricalibert.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AopExpressions {
	@Pointcut("execution(* fr.cedricalibert.aopdemo.dao.*.*(..))")
	public void forDaoPackage() {}
	
	@Pointcut("execution(* fr.cedricalibert.aopdemo.dao.*.get*(..))")
	public void forGettersDaoPackage() {}
	
	@Pointcut("execution(* fr.cedricalibert.aopdemo.dao.*.set*(..))")
	public void forSettersDaoPackage() {}
	
	@Pointcut("forDaoPackage() && !(forGettersDaoPackage() || forSettersDaoPackage())")
	public void forDaoPackageExceptSettersAndGetters() {}
	
}
