package fr.cedricalibert.aopdemo.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDAO {
	public boolean addSillyMember() {
		System.out.println(getClass()+"Doing my db work: adding a membership account");
		return true;
	}
	public void goToSleep() {
		System.out.println(getClass()+"go to sleep");
	}
}
